--
BEGIN;
ALTER TABLE lafsisbio.users
  ADD COLUMN sq_usuario bigint;
ALTER TABLE lafsisbio.users
  ADD COLUMN sq_usuario_externo bigint;
ALTER TABLE lafsisbio.users
  ADD CONSTRAINT fk_sicae_usuario_sq_usuario FOREIGN KEY (sq_usuario) REFERENCES sicae.usuario (sq_usuario) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE lafsisbio.users
  ADD CONSTRAINT fk_sicae_usuario_externo_sq_usuario_externo FOREIGN KEY (sq_usuario_externo) REFERENCES sicae.usuario_externo (sq_usuario_externo) ON UPDATE NO ACTION ON DELETE NO ACTION;
END;
